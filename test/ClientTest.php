<?php

namespace Sparq\Event\Test;

use PHPUnit\Framework\TestCase;
use Sparq\Http\Client;

class ClientTest extends TestCase
{
    public function testSimpleRequestAndResponse()
    {
        $Client = new Client('http://httpbin.org');
        $Result = $Client->run('/get', 'GET');

        $this->assertInstanceOf('Sparq\Http\Client\Result', $Result);
        $this->assertObjectHasAttribute('url', $Result->request);
        $this->assertSame($Result->request->url, 'http://httpbin.org/get');
        $this->assertObjectHasAttribute('method', $Result->request);
        $this->assertObjectHasAttribute('headers', $Result->request);
        $this->assertObjectHasAttribute('options', $Result->request);

        $this->assertObjectHasAttribute('code', $Result->response);
        $this->assertObjectHasAttribute('headers', $Result->response);
        $this->assertObjectHasAttribute('body', $Result->response);

        $this->assertObjectHasAttribute('error', $Result->debug_info);
    }

    public function testPostMultipartFormData()
    {
        $data = [
            'to[0]' => 'some_value',
        ];

        $Client = new Client('http://httpbin.org');
        $Result = $Client->run('/post', 'POST', $data, [
            'Content-Type' => 'multipart/form-data',
        ]);

        $this->assertInstanceOf('Sparq\Http\Client\Result', $Result);
        $this->assertObjectHasAttribute('url', $Result->request);
        $this->assertObjectHasAttribute('method', $Result->request);
        $this->assertObjectHasAttribute('headers', $Result->request);
        $this->assertObjectHasAttribute('options', $Result->request);

        $this->assertObjectHasAttribute('code', $Result->response);
        $this->assertObjectHasAttribute('headers', $Result->response);
        $this->assertObjectHasAttribute('body', $Result->response);

        $this->assertObjectHasAttribute('error', $Result->debug_info);

        $body = json_decode($Result->response->body);

        $this->assertObjectHasAttribute('to[0]', $body->form);
        $this->assertSame($body->form->{'to[0]'}, 'some_value');
    }

    public function testPostApplicationJson()
    {
        $data = [
            'to[0]' => 'some_value',
        ];

        $Client = new Client('http://httpbin.org');
        $Result = $Client->run('/post', 'POST', $data, [
            'Content-Type' => 'application/json',
        ]);

        $this->assertInstanceOf('Sparq\Http\Client\Result', $Result);
        $this->assertObjectHasAttribute('url', $Result->request);
        $this->assertObjectHasAttribute('method', $Result->request);
        $this->assertObjectHasAttribute('headers', $Result->request);
        $this->assertObjectHasAttribute('options', $Result->request);

        $this->assertObjectHasAttribute('code', $Result->response);
        $this->assertObjectHasAttribute('headers', $Result->response);
        $this->assertObjectHasAttribute('body', $Result->response);

        $this->assertObjectHasAttribute('error', $Result->debug_info);

        $body = json_decode($Result->response->body);

        $this->assertObjectHasAttribute('to[0]', $body->json);
        $this->assertSame($body->json->{'to[0]'}, 'some_value');
    }
}
