# Sparq\Http

[![pipeline status](https://gitlab.com/sparq-php/http/badges/master/pipeline.svg)](https://gitlab.com/sparq-php/http/commits/master)
[![Latest Stable Version](https://poser.pugx.org/sparq-php/http/v/stable)](https://packagist.org/packages/sparq-php/http)
[![coverage report](https://gitlab.com/sparq-php/http/badges/master/coverage.svg)](https://gitlab.com/sparq-php/http/commits/master)
[![Total Downloads](https://poser.pugx.org/sparq-php/http/downloads)](https://packagist.org/packages/sparq-php/http)
[![License](https://poser.pugx.org/sparq-php/http/license)](https://packagist.org/packages/sparq-php/http)

A simple to use http request microframework.

### Installation and Autoloading

The recommended method of installing is via [Composer](https://getcomposer.org/).

Run the following command from your project root:

```bash
$ composer require sparq-php/http
```

### Objectives

* Simple to use http requests
* Easy integration with other packages/frameworks
* Fast and low footprint

### Usage

```php
require_once __DIR__ . "/vendor/autoload.php";

use Sparq\Http\RequestBuilder;

$base_url = 'https://targetdomain.com';
$headers = [
	'Content-Type' => 'application/json'
];
$options = []; // Adapter Options

$RequestBuilder = new RequestBuilder($base_url, $headers, $options);
$Result = $RequestBuilder->run('/resourse', 'GET');

$body = $Result->request->body;
```