<?php

namespace Sparq\Http;

use Sparq\Event\EventTrait;

/*
 * Http Client
 */
final class Client
{
    use EventTrait;

    private $base_url;
    private $default_headers;
    private $default_options;

    private $Adapter;
    private $Result;

    /**
     * Construct.
     *
     * @param string $base_url Base URL
     * @param array  $headers  Default headers
     * @param array  $options  Default cURL options
     */
    public function __construct($base_url, array $headers = [], array $options = [])
    {
        $this->base_url = $base_url;
        $this->default_headers = $headers;
        $this->default_options = $options;

        $this->Adapter = '\Sparq\Http\Client\Adapter\Curl';
    }

    /**
     * Get.
     *
     * @param string $key Property name
     *
     * @return any
     */
    public function __get($key)
    {
        return (isset($this->{$key})) ? $this->{$key} : null;
    }

    /**
     * Set Adapter.
     *
     * @param string $adapter_namespace Adapter name space
     */
    public function setAdapter($adapter_namespace)
    {
        if (is_a($adapter_namespace, '\Sparq\Http\Client\Adapter\AbstractAdapter')) {
            throw new Exception('Invalid adapter: '.$adapter_namespace);
        }

        $this->Adapter = $adapter_namespace;

        return true;
    }

    /**
     * Run request.
     *
     * @param string $url        Request endpoint
     * @param string $method     Request method
     * @param array  $parameters Parameters
     * @param array  $headers    Headers
     * @param array  $options    cURL Options
     *
     * @return array Response
     */
    public function run($url, $method, array $parameters = [], array $headers = [], array $options = [])
    {
        /*
         * URL
         */

        if (!is_null($this->base_url)) {
            $url = $this->base_url.'/'.ltrim($url, '/');
        }

        /*
         * Headers
         */

        $headers = array_merge($this->default_headers, $headers);

        /*
         * Options
         */

        $options = array_merge($this->default_options, $options);

        /*
         * Event: before.exec
         */

        $this->emit('before.exec', [
            $url,
            $method,
            $parameters,
            $headers,
            $options,
        ]);

        /*
         * Execute request
         */

        $this->Result = $this->Adapter::run(
            $url,
            $method,
            $parameters,
            $headers,
            $options
        );

        /*
         * Event: after.exec
         */

        $this->emit('after.exec', [
            $this->Result,
        ]);

        return $this->Result;
    }
}
