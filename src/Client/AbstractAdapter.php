<?php

namespace Sparq\Http\Client;

use Closure;
use Exception;

/*
 * Abstract Adapter
 */
abstract class AbstractAdapter
{
    /**
     * Execute request.
     *
     * @param string $url             Request url
     * @param string $method          Request method
     * @param array  $parameters      Request parameters
     * @param array  $headers         Request headers
     * @param array  $adapter_options Adapter Options
     *
     * @return array Result
     */
    abstract public static function run($url, $method, array $parameters = [], array $headers = [], array $adapter_options = []);

    /**
     * Execute several requests at the same time.
     *
     * @param array   $requests               Requests
     * @param int     $simultaneous_downloads Simultaneous downloads
     * @param Closure $callback               Callback
     *
     * @return array Result
     */
    abstract public static function batch(array $requests, $simultaneous_downloads = 5, Closure $callback = null);

    /**
     * Validate content type.
     *
     * @param string $content_type Content type
     *
     * @return string Content type
     */
    final protected static function validateContentType($content_type = null)
    {
        $content_type = strtolower($content_type);

        $available = [
            'application/json',
            'application/x-www-form-urlencoded',
            'multipart/form-data',
        ];

        $found = false;
        foreach ($available as $value) {
            if (false !== strpos($content_type, $value)) {
                $found = true;
            }
        }

        if (!$found) {
            throw new Exception('Invalid content type: '.$content_type);
        }

        return $content_type;
    }

    /**
     * Validate method.
     *
     * @param string $method Method
     *
     * @return string Method
     */
    final protected static function validateMethod($method = null)
    {
        $method = strtoupper($method);

        $available = [
            'GET',
            'POST',
            'PUT',
            'DELETE',
            'HEAD',
        ];

        if (!in_array($method, $available)) {
            throw new Exception('Invalid method: '.$method);
        }

        return $method;
    }
}
