<?php

namespace Sparq\Http\Client\Adapter;

use Closure;
use Sparq\Http\Client\AbstractAdapter;
use Sparq\Http\Client\Result;

/*
 * Curl Adapter
 */
final class Curl extends AbstractAdapter
{
    /**
     * cURLify.
     *
     * @param string $url          Request url
     * @param string $method       Request method
     * @param array  $parameters   Request parameters
     * @param array  $headers      Request headers
     * @param array  $curl_options cURL Options
     *
     * @return array cURL request
     */
    private static function cURLify($url, $method, array $parameters = [], array $headers = [], array $curl_options = [])
    {
        /*
         * Request method
         */

        $method = self::validateMethod($method);

        /*
         * Request content type
         */

        $content_type = null;
        if (isset($headers['Content-Type'])) {
            $content_type = $headers['Content-Type'];
        }

        if (is_null($content_type)) {
            switch ($method) {
                case 'POST':
                    $content_type = 'multipart/form-data';
                    break;
                case 'PUT':
                case 'DELETE':
                    $content_type = 'application/json';
                    break;
                default:
                    $content_type = 'application/x-www-form-urlencoded';
                    break;
            }
        }

        $content_type = self::validateContentType($content_type);

        /*
         * Request headers
         */

        $clone = [];
        foreach ($headers as $key => $value) {
            $clone[] = $key.': '.$value;
        }

        $headers = $clone;

        /*
         * Request parameters & curl options
         */

        switch ($method) {
            case 'HEAD':
                $curl_options['CURLOPT_FOLLOWLOCATION'] = true;
                $curl_options['CURLOPT_NOBODY'] = true;
                break;
            case 'GET':
                if (count($parameters) > 0) {
                    $url .= '?'.http_build_query($parameters);
                }
                $parameters = '';
                break;
            case 'POST':
                if ('application/x-www-form-urlencoded' === $content_type) {
                    $parameters = http_build_query($parameters);
                } elseif ('application/json' === $content_type) {
                    $parameters = (count($parameters) > 0) ? json_encode($parameters) : '{}';
                }
                break;
            case 'DELETE':
            case 'PUT':
                $parameters = (count($parameters) > 0) ? json_encode($parameters) : '{}';
                break;
        }

        /*
         * cURL Options
         */

        $curl_configuration = [
            constant('CURLOPT_URL') => $url,
            constant('CURLOPT_CUSTOMREQUEST') => $method,
            constant('CURLOPT_HTTPHEADER') => $headers,
            constant('CURLOPT_POSTFIELDS') => $parameters,
            constant('CURLOPT_HEADER') => true,
            constant('CURLOPT_RETURNTRANSFER') => true,
            constant('CURLOPT_VERBOSE') => false,
        ];

        foreach ($curl_options as $key => $value) {
            $curl_configuration[constant($key)] = $value;
        }

        return [
            'request' => [
                'url' => $url,
                'method' => $method,
                'headers' => $headers,
                'parameters' => $parameters,
                'options' => $curl_options,
            ],
            'curl_configuration' => $curl_configuration,
        ];
    }

    /**
     * cURL parse content.
     *
     * @param resource $resource cURL Resource
     * @param string   $content  Content
     *
     * @return array cURL Content
     */
    private static function cURLParseContent($resource, $content)
    {
        $curl_info = curl_getinfo($resource);
        $curl_code = (int) $curl_info['http_code'];
        $curl_error = curl_error($resource);

        $curl_body = trim(substr($content, $curl_info['header_size'], strlen($content)));
        $curl_headers = self::getHeaders(trim(substr($content, 0, $curl_info['header_size'])));

        return [
            'info' => $curl_info,
            'error' => $curl_error,
            'code' => $curl_code,
            'headers' => $curl_headers,
            'body' => $curl_body,
        ];
    }

    /**
     * Get Headers.
     *
     * @param string $content cURL content
     *
     * @return array Headers
     */
    private static function getHeaders($content)
    {
        $collection = [];
        foreach (explode("\r\n\r\n", $content) as $key => $value) {
            foreach (explode("\r\n", $value) as $key2 => $value2) {
                if (0 === $key2) {
                    $collection[$key]['protocol'] = $value2;
                    $collection[$key]['headers'] = [];
                } else {
                    $explode = explode(':', $value2);
                    $collection[$key]['headers'][trim($explode[0])] = trim($explode[1]);
                }
            }
            ksort($collection[$key]['headers']);
        }

        return $collection;
    }

    /**
     * Execute requests in batch.
     *
     * @param array $requests               Requests
     * @param int   $simultaneous_downloads Simultaneous downloads
     *
     * @return array cURL request
     */
    public static function batch(array $requests, $simultaneous_downloads = 5, Closure $callback = null)
    {
        /*
         * Configure requests
         */

        $curl_master = curl_multi_init();

        $cursor = 0;
        for ($i = 0; $i < $simultaneous_downloads; ++$i) {
            if ($cursor < count($requests)) {
                /*
                 * cURLify
                 */

                $url = $requests[$cursor]['url'];
                $method = $requests[$cursor]['method'];
                $parameters = $requests[$cursor]['parameters'];
                $headers = $requests[$cursor]['headers'];
                $curl_options = array_merge($requests[$cursor]['curl_options'], ['CURLOPT_PRIVATE' => $cursor]);

                $cURLify = self::cURLify($url, $method, $parameters, $headers, $curl_options);

                $requests[$cursor]['request'] = $cURLify['request'];

                /*
                 * cURL request
                 */

                $requests[$cursor]['handle'] = curl_init();
                curl_setopt_array($requests[$cursor]['handle'], $cURLify['curl_configuration']);
                curl_multi_add_handle($curl_master, $requests[$cursor]['handle']);
            }

            ++$cursor;
        }

        /*
         * Execute cURL
         */

        do {
            while (CURLM_CALL_MULTI_PERFORM == ($multi_exec = curl_multi_exec($curl_master, $running)));

            if (CURLM_OK != $multi_exec) {
                break;
            }

            curl_multi_select($curl_master);

            /*
             * A request was just completed -- find out which one
             */

            while ($done = curl_multi_info_read($curl_master)) {
                /*
                 * Queue next request
                 */

                if ($cursor < count($requests)) {
                    /*
                     * cURLify
                     */

                    $url = $requests[$cursor]['url'];
                    $method = $requests[$cursor]['method'];
                    $parameters = $requests[$cursor]['parameters'];
                    $headers = $requests[$cursor]['headers'];
                    $curl_options = array_merge($requests[$cursor]['curl_options'], ['CURLOPT_PRIVATE' => $cursor]);

                    $cURLify = self::cURLify($url, $method, $parameters, $headers, $curl_options);

                    $requests[$cursor]['request'] = $cURLify['request'];

                    /*
                     * cURL request
                     */

                    $requests[$cursor]['handle'] = curl_init();
                    curl_setopt_array($requests[$cursor]['handle'], $cURLify['curl_configuration']);
                    curl_multi_add_handle($curl_master, $requests[$cursor]['handle']);

                    ++$cursor;
                }

                /*
                 * Callback
                 */

                if (is_callable($callback)) {
                    $curl_content = curl_multi_getcontent($done['handle']);
                    $parse_content = self::cURLParseContent($done['handle'], $curl_content);

                    call_user_func_array($callback, [
                        'request' => $requests[(int) curl_getinfo($done['handle'], CURLINFO_PRIVATE)]['request'],
                        'response' => [
                            'code' => $parse_content['code'],
                            'headers' => $parse_content['headers'],
                            'body' => $parse_content['body'],
                        ],
                        'info' => $parse_content['info'],
                        'error' => $parse_content['error'],
                    ]);
                }

                /*
                 * Remove handler
                 */

                curl_multi_remove_handle($curl_master, $done['handle']);
                curl_close($done['handle']);
            }
        } while ($running);

        curl_multi_close($curl_master);
    }

    /**
     * Execute single request.
     *
     * @param string $url          Request url
     * @param string $method       Request method
     * @param array  $parameters   Request parameters
     * @param array  $headers      Request headers
     * @param array  $curl_options cURL Options
     *
     * @return array cURL request
     */
    public static function run($url, $method, array $parameters = [], array $headers = [], array $curl_options = [])
    {
        /*
         * cURL Options
         */

        $cURLify = self::cURLify($url, $method, $parameters, $headers, $curl_options);

        /*
         * cURL Request
         */

        $curl = curl_init();

        curl_setopt_array($curl, $cURLify['curl_configuration']);

        $curl_content = curl_exec($curl);
        $parse_content = self::cURLParseContent($curl, $curl_content);

        curl_close($curl);

        return new Result($cURLify['request'], [
            'code' => $parse_content['code'],
            'headers' => $parse_content['headers'],
            'body' => $parse_content['body'],
        ], [
            'info' => $parse_content['info'],
            'error' => $parse_content['error'],
        ]);
    }
}
