<?php

namespace Sparq\Http\Client;

use StdClass;

/*
 * Result
 */
final class Result
{
    private $request;
    private $response;
    private $debug_info;

    /**
     * Construct.
     *
     * @param string $request    Request
     * @param array  $response   Response
     * @param array  $debug_info Debug info
     */
    public function __construct(array $request, array $response, array $debug_info = [])
    {
        $this->request = self::sanitize($request);
        $this->response = self::sanitize($response);
        $this->debug_info = self::sanitize($debug_info);
    }

    /**
     * Get property.
     *
     * @param string $key Property name
     *
     * @return any
     */
    public function __get($key)
    {
        return (isset($this->{$key})) ? $this->{$key} : null;
    }

    /**
     * Sanitize.
     *
     * @param array $data Data
     *
     * @return StdClass Data
     */
    private function sanitize($data)
    {
        if (is_scalar($data)) {
            return (mb_strlen($data) > 0) ? $data : null;
        }

        // @toDo : in php7 use is_iterable
        if (!is_array($data) && !is_object($data)) {
            return null;
        }

        if (0 === count($data)) {
            return null;
        }

        $clone = null;
        if (null === $clone && is_array($data)) {
            $is_object = false;

            $clone = [];
            foreach ($data as $key => $value) {
                if (false === $is_object && !is_int($key)) {
                    $is_object = true;
                }
                $clone[$key] = self::sanitize($value);
            }

            if ($is_object) {
                $clone = (object) $clone;
            }
        }

        if (null === $clone && is_object($data)) {
            $clone = new StdClass();
            foreach ($data as $key => $value) {
                $clone->$key = self::sanitize($value);
            }
        }

        return $clone;
    }
}
